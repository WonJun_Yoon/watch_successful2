package com.example.markyoon.sample_wearable;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.*;

import static android.provider.BaseColumns._ID;
import static com.example.markyoon.sample_wearable.Constants.INDEX4;
import static com.example.markyoon.sample_wearable.Constants.TABLE_NAME;
import static com.example.markyoon.sample_wearable.Constants.INDEX3;
import static com.example.markyoon.sample_wearable.Constants.INDEX1;
import static com.example.markyoon.sample_wearable.Constants.INDEX2;
import static com.example.markyoon.sample_wearable.Constants.TABLE_NAME;
import static com.example.markyoon.sample_wearable.Constants.TIMER1;
import static com.example.markyoon.sample_wearable.Constants.TIMER2;
public class MyActivity extends Activity {

    private TextView mTextView;
    private MainPage mainpage;
    private PersonPage personpage;
    private Status status;
    private Boolean TF = false;
    private Boolean TF1 = false;
    private long second =0;
    private String[] FROM = {TIMER1};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
            }
        });

        // get a ViewPager reference and set adapter for it
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(this);
        pager.setAdapter(adapter);
    }


    public class ViewPagerAdapter extends PagerAdapter {
        // sample names
        private String[] callNumbers = {"sleep", "feed", "clean", "walk"};

        /**
         * Context
         */
        private Context mContext;

        /**
         * Initialize
         *
         * @param context
         */
        public ViewPagerAdapter(Context context) {
            mContext = context;
        }

        /**
         * Count of pages
         */
        public int getCount() {
            return 3;
        }

        /**
         * Called before a page is created
         */
        public Object instantiateItem(View pager, int position) {
            if (position == 0) {
                mainpage = new MainPage(mContext);
                // add to the ViewPager
                ViewPager curPager = (ViewPager) pager;
                curPager.addView(mainpage, position);
                return mainpage;
            } else if (position == 1) {
                PersonPage page = new PersonPage(mContext);

                page.setSleepButton(callNumbers[position]);
                page.setFeedButton(callNumbers[position]);
                page.setCleanButton(callNumbers[position]);
                page.setWalkButton(callNumbers[position]);

                ViewPager curPager = (ViewPager) pager;
                curPager.addView(page, position);
                return page;
            } else {

                status = new Status(mContext);
                if(TF1)
                status.timer_sec += second+ (((System.currentTimeMillis()) - (readDataBase(position))))/1000;
                // add to the ViewPager
                ViewPager curPager = (ViewPager) pager;
                curPager.addView(status, position);
                TF1 = true;
                return status;
            }
        }

        /**
         * Called to remove the page
         */
        public void destroyItem(View pager, int position, Object view){
            writeDataBase(System.currentTimeMillis(),position);
            if(position ==2) {
                second = status.timer_sec;
                Toast.makeText(mContext, "second :" + second, Toast.LENGTH_SHORT).show();
            }
            ((ViewPager) pager).removeView((View) view);
        }

        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        public void finishUpdate(View view) {

        }

        public void restoreState(Parcelable p, ClassLoader c) {

        }

        public Parcelable saveState() {
            return null;
        }

        public void startUpdate(View view) {

        }

        String hi = Integer.toString(2) + "hi";
    }

    private void writeDataBase(long time, int position) {
        TF = true;
        // TODO Auto-generated method stub
        StorageOfStatus statusDataBase = new StorageOfStatus(this);
        ContentValues values = new ContentValues();
        SQLiteDatabase db = statusDataBase.getWritableDatabase();
        try {
                values.put(TIMER1,time);
            db.insertOrThrow(TABLE_NAME,null,values);
        } catch (Throwable e) {
                Toast.makeText(this,"Write Error",Toast.LENGTH_SHORT).show();
        }
        finally {
            statusDataBase.close();
        }
    }

    private long readDataBase(int position) {
        if(!TF) return 0;
        // TODO Auto-generated method stub
        Cursor cursor;
        long time = 0;

        StorageOfStatus statusDataBase = new StorageOfStatus(this);
        SQLiteDatabase db = statusDataBase.getReadableDatabase();
        cursor = db.query(TABLE_NAME, FROM, null, null, null, null, null);
        cursor.moveToLast();
        try {
                time = cursor.getLong(0);

        } catch (Exception e) {
            Toast.makeText(this, "Exception8 : " + e.toString(), Toast.LENGTH_LONG).show();
        } finally {
            statusDataBase.close();
        }

        return time;
    }
}
