package com.example.markyoon.sample_wearable;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class Status extends LinearLayout {
    Context mContext;
    private TimerTask second;
    private TextView timer_text;
    private final Handler handler = new Handler();
    public long timer_sec_1=0;
    public long timer_sec_2=0;
    public long timer_sec_3=0;
    public long timer_sec_4=0;

    private TimerTask tt1;
    private TimerTask tt2;
    private TimerTask tt3;
    private TimerTask tt4;

    private TextView hunger_text;
    private TextView clean_text;
    private TextView boring_text;
    private TextView sleep_text;

    private final Handler handler1 = new Handler();
    private final Handler handler2 = new Handler();
    private final Handler handler3 = new Handler();
    private final Handler handler4 = new Handler();


    public static final int CALL_NUMBER = 1001;

    public Status(Context context) {
        super(context);

        init(context);
    }

    public Status(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    private void init(Context context) {
        mContext = context;

        // inflate XML layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.status, this, true);
        hunger_text = (TextView) findViewById(R.id.hunger);
        clean_text = (TextView) findViewById(R.id.clean);
        boring_text = (TextView) findViewById(R.id.boring);
        sleep_text = (TextView) findViewById(R.id.sleepy);

        testStart_1();
        testStart_2();
        testStart_3();
        testStart_4();

    }

    public void testStart_1() {


        tt1 = new TimerTask() {

            @Override
            public void run() {
                Log.i("Test", "Timer start");
                Update_1();
                timer_sec_1++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(tt1, 0, 1000);
    }
    protected void Update_1() {
        Runnable updater = new Runnable() {

            public void run() {hunger_text.setText("Timer = "+timer_sec_1 + "초");
            }

        };        handler1.post(updater);
    }

    public void testStart_2() {

        tt2 = new TimerTask() {

            @Override
            public void run() {
                Log.i("Test", "Timer start");
                Update_2();
                timer_sec_2++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(tt2, 0, 2000);
    }
    protected void Update_2() {
        Runnable updater = new Runnable() {

            public void run() {clean_text.setText("Timer = "+timer_sec_2 + "초");
            }

        };        handler2.post(updater);
    }


    public void testStart_3() {


        tt3 = new TimerTask() {

            @Override
            public void run() {
                Log.i("Test", "Timer start");
                Update_3();
                timer_sec_3++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(tt3, 0, 3000);
    }
    protected void Update_3() {
        Runnable updater = new Runnable() {

            public void run() {boring_text.setText("Timer = "+timer_sec_3 + "초");
            }

        };        handler3.post(updater);
    }



    public void testStart_4() {


        tt4 = new TimerTask() {

            @Override
            public void run() {
                Log.i("Test", "Timer start");
                Update_4();
                timer_sec_4++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(tt4, 0, 4000);
    }
    protected void Update_4() {
        Runnable updater = new Runnable() {

            public void run() {sleep_text.setText("Timer = "+timer_sec_4 + "초");
            }

        };        handler4.post(updater);
    }



}

